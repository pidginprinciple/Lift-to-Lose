package com.android.qgilashp.lifttolose;


import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.WallpaperManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
        import android.os.Environment;
        import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.TextSwitcher;
import android.widget.TextView;
        import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.android.qgilashp.lifttolose.Webviews.mainInstruction;
import com.android.qgilashp.lifttolose.Webviews.mainWebview;

import au.com.bytecode.opencsv.CSVReader;
        import au.com.bytecode.opencsv.CSVWriter;
import de.passsy.holocircularprogressbar.HoloCircularProgressBar;


import java.io.*;
        import java.util.ArrayList;
        import java.util.List;
import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: pidgin
 * Date: 11/12/13
 * Time: 1:27 PM
 * To change this template use File | Settings | File Templates.
 */
public class lifttoloser extends ActionBarActivity {

    private EditText testText;
    //private com.cesarferreira.androidbootstrap.BootstrapButton addnoW;
    private Button helpButton;
    private ImageButton aboutButton;
    private TextSwitcher textView;
    private TextView quotes;
    private Switch nextSlide;
    private TextSwitcher nextSet;
    private de.passsy.holocircularprogressbar.HoloCircularProgressBar currProg;
    private ObjectAnimator anim;
    protected boolean animEnd = false;
    //@InjectView(R.id.addnow) Button addnoW;
    String test = "Success";
    //public Integer currSet= 0;
    private int start;
    private int end;
    private String filename;
    private String buttonType;
    private String floatsave;
    private int currLine;
    private final String MY_PREFS_NAME = "files";
    public int currSet;
    private String[] sets= new String[50];
    private int counter =0;
    private int arrit = 0;
    private float increment = .20f;
    private String[] quotearray;
    private static final Random rgenerator = new Random();
    private static int setnumber = 0;
    private static int[] monarr = new int[] {5,5,5,5,5};
    private static int[] wedarr = new int[] {4,4,4,4};
    private static int[] friarr = new int[] {5,5,5,5,3,8};

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.setContentView(R.layout.mainlayout);
        //addnoW = (com.cesarferreira.androidbootstrap.BootstrapButton) findViewById(R.id.addnow);
        //testText = (EditText) findViewById(R.id.addition);
        //testText.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI);
        helpButton = (Button) findViewById(R.id.helpbutton);
        aboutButton = (ImageButton) findViewById(R.id.imageView);
        textView = (TextSwitcher) findViewById(R.id.textView2  );
        quotes = (TextView) findViewById(R.id.quote);
        nextSlide = (Switch) findViewById(R.id.slidefornext);
        nextSet = (TextSwitcher) findViewById(R.id.main_textswitcher);
        currProg = (de.passsy.holocircularprogressbar.HoloCircularProgressBar) findViewById(R.id.progressbar);

        currProg.setProgressColor(Color.YELLOW);
        currProg.setProgressBackgroundColor(Color.TRANSPARENT);

        quotearray = getResources().getStringArray(R.array.quotes);
        String randquote = quotearray[rgenerator.nextInt(quotearray.length)];
        quotes.setText(randquote);

        getInfo(); 
        currSet=start;
        setnumber = (end-start);
        if(setnumber==3)
            increment = .25f;
        animate(currProg, null, increment, 1000);
        sets = readNext(filename, start);

        helpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), mainInstruction.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("page", 4);
                getBaseContext().startActivity(intent);
            }
        });

        aboutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                Intent intent = new Intent(getBaseContext(), mainWebview.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getBaseContext().startActivity(intent);
            }
        });

        nextSet.setFactory(new ViewSwitcher.ViewFactory() {

            public View makeView() {
                TextView myText = new TextView(lifttoloser.this);
                myText.setGravity(Gravity.CENTER);

                FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
                        FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT,
                        Gravity.CENTER);
                myText.setLayoutParams(params);

                myText.setTextSize(36);
                myText.setTextColor(Color.WHITE);
                return myText;
            }
        });

        textView.setFactory(new ViewSwitcher.ViewFactory() {

            public View makeView() {
                TextView myText = new TextView(lifttoloser.this);
                myText.setGravity(Gravity.CENTER);

                FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
                        FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT,
                        Gravity.CENTER);
                myText.setLayoutParams(params);

                myText.setTextSize(20);
                myText.setTextColor(Color.WHITE);
                return myText;
            }
        });

        final WallpaperManager wallpaperManager = WallpaperManager.getInstance(this);
        final Drawable wallpaperDrawable = wallpaperManager.getFastDrawable();
        getWindow().setBackgroundDrawable(wallpaperDrawable);

        Animation slidedown = AnimationUtils.loadAnimation(this, android.R.anim.slide_in_left);
        Animation slideright = AnimationUtils.loadAnimation(this, android.R.anim.slide_out_right);

        nextSet.setInAnimation(slidedown);
        nextSet.setOutAnimation(slideright);
        nextSet.setText("Swipe to Start");

        textView.setInAnimation(slidedown);
        textView.setOutAnimation(slideright);
       /* addnoW.setOnClickListener(new View.OnClickListener() {


    @Override
    public void onClick(View v) {
        //To change body of implemented methods use File | Settings | File Templates.
                Toast.makeText(testdisplay.this, test, Toast.LENGTH_SHORT).show();
kk
        }
    }); */
        //  Injector.inject(this);
        /*addnoW.setOnClickListener(new View.OnClickListener() {
          //  @Override
            public void onClick(View v) {
                String filename = "teeeeeeest.csv";
                String content = testText.getText().toString();
                Integer integer = Integer.parseInt(content) + 2;
                String[] result =  new String[]{"" + String.valueOf(integer) , "workout", "is", "cool", "and", "so", "are", "you", "haha"};
                createFile(filename, result);
                //textView.setText("The answer is " + readFile(filename));
               // readFile(filename);
                Toast.makeText(lifttoloser.this, test, Toast.LENGTH_SHORT).show();
                String name = "poop";
            }
        });*/

        nextSlide.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                String startfile = filename;
                if(isChecked) {
                    if (counter == (end - start)) {
                        Toast lastset = Toast.makeText(lifttoloser.this, "Last set. Swipe again to finish.", Toast.LENGTH_SHORT);
                        lastset.setGravity(Gravity.CENTER, 0, 60);
                        lastset.show();

                    }
                    if (counter == ((end - start) + 1)) {
                        finishSet();
                    }
                    //readNext(startfile, start);
                    //isChecked = false;


                    if (counter <= ((end - start) )){
                        if(currLine ==0) {
                            nextSet.setText(sets[currSet] + " lbs");
                        }else{
                            nextSet.setText(sets[currSet + 1] +" lbs");
                        }
                        if (setnumber == 4) {
                            textView.setVisibility(View.VISIBLE);
                            textView.setText("x " + Integer.toString(monarr[arrit]));
                        } else if (setnumber == 3) {
                            textView.setVisibility(View.VISIBLE);
                            textView.setText("x " + Integer.toString(wedarr[arrit]));
                        } else if (setnumber == 5) {
                            textView.setVisibility(View.VISIBLE);
                            textView.setText("x " + Integer.toString(friarr[arrit]));
                        }
                        if (counter < (end - start) ){
                            arrit++;
                        }
                        currSet++;
                        counter++;
                    }
                   // textView.setVisibility(View.VISIBLE);
                   // textView.setText("x " + Integer.toString(setnumber));

                    // animate progress bar

                    if(setnumber == 3) {
                        increment = increment + .20f;
                    }
                    else if(setnumber == 4){
                        increment = increment + .17f;
                    } else if(setnumber ==5){
                        increment = increment + .15f;
                    }
                    animate(currProg, null, increment, 1000);
                    currProg.setMarkerProgress(increment);
                    currProg.setProgressColor(Color.parseColor("#FF75D1FF"));
                    currProg.setProgressBackgroundColor(Color.TRANSPARENT);
                }
                Log.v("On slider= ",String.valueOf(currSet) );
                nextSlide.setChecked(false);
            }
        });

        aboutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                Intent intent = new Intent(getBaseContext(), mainWebview.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getBaseContext().startActivity(intent);
            }
        });

    }

    @Override
    public void onPause(){
        super.onPause();
        floatsave = Float.toString(increment);

    }

    @Override
    public void onResume(){
        super.onResume();
        Log.v("On resume= ",String.valueOf(currSet) );
        int resarrit = arrit;
        int rescurrSet = currSet;
        if(counter>0) {

            nextSet.setText(sets[rescurrSet--] + " lbs");
           // Log.v("On nextset= ", String.valueOf(rescurrSet));

            if (setnumber == 4) {
                if(counter == ((end - start)+1)){
                    textView.setVisibility(View.VISIBLE);
                    textView.setText("x " + Integer.toString(monarr[resarrit]));
                }else {
                    textView.setVisibility(View.VISIBLE);
                    textView.setText("x " + Integer.toString(monarr[--resarrit]));
                }

            } else if (setnumber == 3) {
                if(counter == ((end - start)+1)){
                    textView.setVisibility(View.VISIBLE);
                    textView.setText("x " + Integer.toString(wedarr[resarrit]));
                }else {
                    textView.setVisibility(View.VISIBLE);
                    textView.setText("x " + Integer.toString(wedarr[--resarrit]));
                }

            } else if (setnumber == 5) {
                Log.v("resarrit= ", String.valueOf(counter));
                    if(counter == ((end - start)+1)){
                        textView.setVisibility(View.VISIBLE);
                        textView.setText("x " + Integer.toString(friarr[resarrit]));
                    }else {
                        textView.setVisibility(View.VISIBLE);
                        textView.setText("x " + Integer.toString(friarr[--resarrit]));
                    }

            }

        }
        if (floatsave != null)
            increment = Float.parseFloat(floatsave);
        if(increment==.20f){
            animate(currProg, null, increment, 1000);
            currProg.setMarkerProgress(increment);
        }else{
        animate(currProg, null, increment, 1000);
        currProg.setMarkerProgress(increment);
        }
    }


    @Override
    protected void onSaveInstanceState(Bundle icicle) {
        super.onSaveInstanceState(icicle);
        icicle.putInt("currset", currSet);
        icicle.putInt("counter", counter);
        icicle.putInt("increment", (int)increment);
        icicle.putInt("currline", currLine);
        icicle.putInt("arrit", arrit);
        icicle.putString("floatsave", floatsave);

    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        currSet = (int) savedInstanceState.getSerializable("currset");
        counter = (int) savedInstanceState.getSerializable("counter");
        increment = (int) savedInstanceState.getSerializable("increment");
        currLine = (int) savedInstanceState.getSerializable("currline");
        arrit = (int) savedInstanceState.getSerializable("arrit");
        floatsave = (String) savedInstanceState.getSerializable("floatsave");

    }

    /**
     * Prevent the variables from being saved if back button is pressed
     * @param keyCode
     * @param event
     * @return
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if ((keyCode == KeyEvent.KEYCODE_BACK))
        {
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    private void finishSet(){
        Intent finish = new Intent(this, liftHub.class);
        finish.putExtra(buttonType, true);
        finish.putExtra("file", filename);
        finish.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        this.startActivity(finish);
        finish();
    }



    public String[] readNext(String fileName, int startAr){
        //String line = null;
        String[] row = null;
        List<String[]> message = new ArrayList<String[]>();
        //BufferedReader buffer = null;
        FileReader reader;
        CSVReader csvreader;
        File gpxfile;
        SharedPreferences files = getSharedPreferences(MY_PREFS_NAME, 0);
        fileName = files.getString("currfile", "");
        try{
            File root = new File(Environment.getExternalStorageDirectory(), "Lift to Lose");

            if(!root.exists()){
                Toast.makeText(lifttoloser.this, "Folder does not Exist", Toast.LENGTH_SHORT).show();

            }
            gpxfile = new File(root, fileName);
            reader = new FileReader(gpxfile);
            //buffer = new BufferedReader(reader);
            if (currLine == 0){
                csvreader = new CSVReader(reader);
            }else {
                csvreader = new CSVReader(reader, ',', CSVWriter.DEFAULT_QUOTE_CHARACTER, currLine);


            }

        row = csvreader.readNext();
            /*while((line=buffer.readLine())!=null){
               message += line;
            }  */

            //for(int i= startAr;i<= endAr;i++){
              //  int j = 0;
               // row[j] = line[i];
               // j++;
                //String [] bum = new String[4];
                /*for(int i=0;i<bum.length;i++){
                       bum = row;
                } */



                //message.add(bum);
           // }

           // nextSet.setText(line[currSet]);
           // currSet++;
            try {
                csvreader.close();
            } catch (IOException e1) {
                e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            //return message;
        }catch (IOException e){
            e.printStackTrace();

        }
        return row;
    }

    private void animate(final HoloCircularProgressBar progressBar, final Animator.AnimatorListener listener) {
		final float progress = (float) (Math.random() * 2);
		int duration = 3000;
		animate(progressBar, listener, progress, duration);
	}

	private void animate(final HoloCircularProgressBar progressBar, final Animator.AnimatorListener listener,
			final float progress, final int duration) {

		anim = ObjectAnimator.ofFloat(progressBar, "progress", progress);
		anim.setDuration(duration);

		anim.addListener(new Animator.AnimatorListener() {

			@Override
			public void onAnimationCancel(final Animator animation) {
			}

			@Override
			public void onAnimationEnd(final Animator animation) {
				progressBar.setProgress(progress);
			}

			@Override
			public void onAnimationRepeat(final Animator animation) {
			}

			@Override
			public void onAnimationStart(final Animator animation) {
			}
		});
		if (listener != null) {
			anim.addListener(listener);
		}
		anim.reverse();
		anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

			@Override
			public void onAnimationUpdate(final ValueAnimator animation) {
				progressBar.setProgress((Float) animation.getAnimatedValue());
			}
		});
		progressBar.setMarkerProgress(progress);
		anim.start();
	}



    private void getInfo(){
        
        Intent testInt = getIntent();
        Bundle extras = testInt.getExtras();

        if(extras!=null) {
            filename = (String) extras.get("filestart");
            buttonType = (String) extras.get("Set");
            start = extras.getInt("start");
            end = extras.getInt("end");

        }

        SharedPreferences getLine = getSharedPreferences(MY_PREFS_NAME, 0);
        currLine = getLine.getInt(filename+"line", 0);// set current line for session
    }

}
