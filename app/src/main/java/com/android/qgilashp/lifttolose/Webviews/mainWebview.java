package com.android.qgilashp.lifttolose.Webviews;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

/**
 * Created by Flor on 3/1/2015.
 */
public class mainWebview extends Activity {

    private WebView mWebView;
    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        mWebView = new WebView(this);
        final Activity activity = this;

        mWebView.setWebViewClient(new WebViewClient() {
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(activity, description, Toast.LENGTH_SHORT).show();
            }
        });

        mWebView.loadUrl("file:///android_asset/mainhelp.html");
        setContentView(mWebView);
    }
}

