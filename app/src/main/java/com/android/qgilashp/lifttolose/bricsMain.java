package com.android.qgilashp.lifttolose;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.os.Bundle;
import android.widget.TextView;

import com.android.qgilashp.lifttolose.Webviews.mainInstruction;
import com.android.qgilashp.lifttolose.Webviews.mainWebview;

import java.io.File;
import java.util.HashMap;

/**
 * Created by pidgin on 8/12/14.
 */
public class bricsMain extends Activity {
    public String currentFile;
    private ListView mainList;
    private TextView emptyList;
    private Button showFile;
    private Button createFile;
    private Button helpButton;
    private ImageButton aboutButton;
    private final String MY_PREFS_NAME = "files";
    private HashMap<String, String> fileMap = new HashMap<String, String>();
    //String[] fileList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.setContentView(R.layout.mainmenu);
        mainList = (ListView) findViewById(R.id.listFiles);
        emptyList = (TextView) findViewById(R.id.emptyList);
        showFile = (Button) findViewById(R.id.button);
        createFile = (Button) findViewById(R.id.addfile);
        helpButton = (Button) findViewById(R.id.helpbutton);
        aboutButton = (ImageButton) findViewById(R.id.imageView);
        //cheatcodes();
        showFiles();
        setButtons();

    }

	// test function to bypass next day lock
	/*
    public void cheatcodes(){
        SharedPreferences.Editor buttons = getSharedPreferences(MY_PREFS_NAME, 0).edit();
        buttons.clear();
        buttons.putString("George.csv"+"nextdate", "11-10-2014");
        buttons.apply();
    }
	*/

    public void showFiles(){
        File files = null;
        if(checkStorageType()==true) {
            files = new File(Environment.getExternalStorageDirectory(), "Lift to Lose");
            files.mkdirs();
        } else {
           files = new File(getApplicationContext().getFilesDir() + "/Lift to Lose");
           files.mkdirs();
        }
        String[] fileList = files.list();
        SharedPreferences getDate = getSharedPreferences(MY_PREFS_NAME, 0);
        //String[] dateStarted = new String[];
        Log.v("file list =", String.valueOf(fileList));
        if(fileList.length==0) {
            emptyList.setVisibility(View.VISIBLE);
            mainList.setVisibility(View.GONE);
            //String[] dateCreated = files.list().lastModified();
            //fileList = getApplicationContext().fileList();
            //ArrayAdapter<String> setFiles = new ArrayAdapter<String>(this, R.layout.mainmenu, fileList);
        }else {
            for (int i = 0; i < fileList.length; i++) {

                String dateStarted = getDate.getString(fileList[i], "Did not start yet.");

                //fileList[i] = fileList[i].replace(".txt", "");
                fileMap.put(fileList[i], dateStarted);

            }
            // mainList.setAdapter(setFiles);
            //ArrayAdapter<String> setFiles = new ArrayAdapter<String>(this, R.layout.listtext, fileList);
            //mainList.setAdapter(setFiles);
            mainAdapter adapter = new mainAdapter(this, fileMap);
            mainList.setAdapter(adapter);
        }
    }

    public void setButtons(){

        helpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), mainInstruction.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("page", 0);
                getBaseContext().startActivity(intent);
            }
        });

        aboutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                Intent intent = new Intent(getBaseContext(), mainWebview.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getBaseContext().startActivity(intent);
            }
        });

        createFile.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(getBaseContext(), liftForm.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getBaseContext().startActivity(intent);
            }
        });
    }

    public boolean checkStorageType(){
        boolean mExternalStorageAvailable = false;
        boolean mExternalStorageWriteable = false;
        String state = Environment.getExternalStorageState();

        if (Environment.MEDIA_MOUNTED.equals(state)) {
            // We can read and write the media
            mExternalStorageAvailable = mExternalStorageWriteable = true;
            return true;
        } else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            // We can only read the media
            mExternalStorageAvailable = true;
            mExternalStorageWriteable = false;
            return false;
        } else {
            // Something else is wrong. It may be one of many other states, but all we need
            //  to know is we can neither read nor write
            mExternalStorageAvailable = mExternalStorageWriteable = false;
            return false;
        }
    }
}
