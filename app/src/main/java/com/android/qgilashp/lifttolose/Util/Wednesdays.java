package com.android.qgilashp.lifttolose.Util;

import java.util.List;

/**
 * Created by pidgin on 10/20/14.
 */
public class Wednesdays {
    //private final Double percentInc = 1.1;



    public String[] wedBench(List<String[]> benchi){
        String[] wedbenarray = new String[52];
        String [] temparray;
        temparray = benchi.get(0);
        int j = 0;
        for(int i=0;i<wedbenarray.length;i++){
            if(i<4){

                if(i==3) {
                    j++;
                    wedbenarray[3] = temparray[2];
                    //wedState = "press";
                    j=j + 1;

                }else{
                    wedbenarray[i] = temparray[j++];
                }

            }else{
                if( i != 4 && i%4==0  && j<temparray.length) {
                    wedbenarray[i-1] = temparray[j - 2];
                    wedbenarray[i] = temparray[j-4];
                    //wedState = "press";
                    if(j<56) {j = j + 2;}

                }else{
                    if( j<temparray.length )
                        wedbenarray[i] = temparray[j++];

                }
            }
        }
        wedbenarray[49] = temparray[56];
        wedbenarray[50] = temparray[57];
        wedbenarray[51] = temparray[57];
        return wedbenarray;
    }

    public String[] calcWed(int calculate, Double percentInc) {
        String[] wedArray = new String[52];
        int pmult = 3;
        Double prevPress = 0.0;
        Double iPress = 0.0;
        Double temp = 0.0;
        String[] temparray;
        Double adjust = .9345;

        for(int i=0;i<wedArray.length;i++){

            if(i !=0 && i%4==0) pmult = 5;

            if(i<4){
                iPress = (calculate*(Math.pow(adjust,4)))*(pmult * .125);
                temp = 5*(Math.floor(Math.abs(iPress/5)));
                wedArray[i] = temp.toString();

            }else{
                if(i%4==0){
                    String prevString = wedArray[i-1];
                    prevPress = Double.parseDouble(prevString);
                    iPress = (prevPress * percentInc)*(pmult * .125);

                }else{
                    iPress =  (prevPress * percentInc) * (pmult * .125);
                }

                temp = 5*(Math.floor(Math.abs(iPress/5)));
                wedArray[i] = temp.toString();

            }
            pmult++;

        }
        return wedArray;
    }
}
