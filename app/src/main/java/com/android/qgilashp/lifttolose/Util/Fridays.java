package com.android.qgilashp.lifttolose.Util;

import java.util.List;

/**
 * Created by pidgin on 10/20/14.
 */
public class Fridays {
    private final Double percentInc = 1.07;


    public String[] calcBFriday(List<String[]> monbench) {

        String temparray[] = new String[60];
        String fBench[] = new String[72];
        temparray = monbench.get(0);
        double temp;
        String parseBench;
        int a = 2, b = 3, c = 4, d = 1;

        for (int i = 0; i < 71; i++) {
            if (i < 6) {
                fBench[i] = temparray[i];
                if (i == 4) {
                    parseBench = temparray[i];
                    temp = Double.parseDouble(parseBench);
                    temp = temp * percentInc;
                    temp = 5 * (Math.floor(Math.abs(temp / 5)));
                    fBench[i] = String.valueOf(temp);
                }
                if (i == 5) {
                    fBench[i] = fBench[i - 2];
                }
            } else {
                if (i != 6 && i % 6 == 0) {
                    fBench[i] = temparray[i - a];
                    parseBench = temparray[i - b];
                    temp = Double.parseDouble(parseBench);
                    temp = temp * percentInc;
                    temp = 5 * (Math.floor(Math.abs(temp / 5)));
                    fBench[i - 2] = String.valueOf(temp);
                    fBench[i - 1] = temparray[i - c];
                    a++;
                    b++;
                    c++;
                    d++;
                }/*else if(i%6==0){
    				fBench[i-1] = fBench[i-3];
                    fBench[i] = temparray[i-1];
    			}*/ else {
                    if (i == 6) fBench[i] = temparray[i - 1];
                    else if (i == 70) {
                        parseBench = temparray[i - 11];
                        temp = Double.parseDouble(parseBench);
                        temp = temp * percentInc;
                        temp = 5 * (Math.floor(Math.abs(temp / 5)));
                        fBench[i] = String.valueOf(temp);
                        fBench[i + 1] = temparray[i - 12];
                    } else fBench[i] = temparray[i - d];

                }
            }
        }
        return fBench;
    }


}