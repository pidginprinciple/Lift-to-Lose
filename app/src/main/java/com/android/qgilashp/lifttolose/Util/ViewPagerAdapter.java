package com.android.qgilashp.lifttolose.Util;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.qgilashp.lifttolose.R;

/**
 * Created by Flor on 3/2/2015.
 */
public class ViewPagerAdapter extends PagerAdapter{

        Context mContext;
        LayoutInflater mLayoutInflater;
    int[] mResources = {
            R.drawable.maininstructions,
            R.drawable.formhelp,
            R.drawable.hubhelp,
            R.drawable.hubhelp2,
            R.drawable.lifthelp

    };

        public ViewPagerAdapter(Context context) {
            mContext = context;
            mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return mResources.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((LinearLayout) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View itemView = mLayoutInflater.inflate(R.layout.inflatepager, container, false);

            ImageView imageView = (ImageView) itemView.findViewById(R.id.imageView);
            imageView.setImageResource(mResources[position]);
            imageView.setScaleType(ImageView.ScaleType.FIT_XY);

            container.addView(itemView);

            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((LinearLayout) object);
        }
    }

