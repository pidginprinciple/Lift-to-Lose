package com.android.qgilashp.lifttolose.Util;

/**
 * Created by pidgin on 10/20/14.
 */
public class Mondays {
    private final Double percentInc = 1.070;
    double adjust = .9345;
    private Double iBench = 0.0;
    private Double fBench;

    public String[] calcMon(int monCalc){

        String[] sBench = new String[60];
        int mult = 1;
        Double prevNum = 0.0;
        for(int i=0;i<60;i++){

            if(i%5==0) mult = 4;

            if(i<5){
                iBench = (monCalc*(Math.pow(adjust,4)))*(mult * .125);
                fBench = 5*(Math.floor(Math.abs(iBench/5)));
                sBench[i] = fBench.toString();
                ;
            }else{
                if(i%5==0){
                    String prevString = sBench[i-1];
                    prevNum = Double.parseDouble(prevString);
                    iBench = (prevNum * percentInc)*(mult * .125);

                }else{
                    iBench =  (prevNum * percentInc) * (mult * .125);
                }

                fBench = 5*(Math.floor(Math.abs(iBench/5)));
                sBench[i] = fBench.toString();

            }
            mult++;

        }
        return sBench;
    }
}
