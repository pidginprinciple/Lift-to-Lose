package com.android.qgilashp.lifttolose;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.qgilashp.lifttolose.Util.*;
import com.android.qgilashp.lifttolose.Webviews.mainInstruction;
import com.android.qgilashp.lifttolose.Webviews.mainWebview;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.widget.ImageButton;
import android.widget.Toast;

import au.com.bytecode.opencsv.CSVWriter;

/**
 * Created by pidgin on 9/28/14.
 */
public class liftForm extends Activity {

	private EditText benchText;
    private EditText fileName;
    private EditText squatText;
    private EditText rowText;
    private EditText pressText;
    private EditText deadText;
	private Button calculate;
    private Button helpButton;
    private ImageButton aboutButton;
    private final Double percentInc = 1.035; //1.025

	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);

		this.setContentView(R.layout.filloutform);
		squatText = (EditText) findViewById(R.id.Squat);
		benchText = (EditText) findViewById(R.id.Bench);
		rowText = (EditText) findViewById(R.id.Row);
		pressText = (EditText) findViewById(R.id.Press);
		deadText = (EditText) findViewById(R.id.Deadlift);
		calculate = (Button) findViewById(R.id.button);
        helpButton = (Button) findViewById(R.id.helpbutton);
        aboutButton = (ImageButton) findViewById(R.id.imageView);
        fileName = (EditText) findViewById(R.id.filename);

		calculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // String filename = "teeeeeeest.csv";
            	if(squatText.getText().toString().isEmpty() || benchText.getText().toString().isEmpty() ||
            		rowText.getText().toString().isEmpty() || pressText.getText().toString().isEmpty() ||
            		deadText.getText().toString().isEmpty()){

            		AlertDialog.Builder alert = new AlertDialog.Builder(liftForm.this);
            		alert.setTitle("ERROR:INCOMPLETE FORM");
            		alert.setMessage("Please fill out all fields.");
            		alert.setCancelable(true);
            		alert.setNeutralButton("Ok", new DialogInterface.OnClickListener(){
            			public void onClick(DialogInterface dialog, int id){
            				dialog.cancel();
            			}
            		});
            		AlertDialog alert1 = alert.create();
            		alert1.show();
            	}else{
               	    int benchInt = Integer.parseInt(benchText.getText().toString());
                    int squatInt = Integer.parseInt(squatText.getText().toString());
                    int rowInt = Integer.parseInt(rowText.getText().toString());
                    int pressInt = Integer.parseInt(pressText.getText().toString());
                    int deadInt = Integer.parseInt(deadText.getText().toString());
                    calcSchedule(squatInt, benchInt, rowInt, pressInt, deadInt);
                    Toast.makeText(liftForm.this, "Successfully created.  Look under the folder 'Lift to Lose' to check the numbers.", Toast.LENGTH_LONG).show();
                    backtoMain();
                }
            }
        });

        helpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), mainInstruction.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("page", 1);
                getBaseContext().startActivity(intent);
            }
        });

        aboutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                Intent intent = new Intent(getBaseContext(), mainWebview.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getBaseContext().startActivity(intent);
            }
        });

	}

	public void backtoMain(){
		Intent finish = new Intent(this, bricsMain.class);
        finish.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        this.startActivity(finish);
        finish();
	}

	public void calcSchedule(Integer Squat, Integer Bench, Integer Row,
							Integer oPress, Integer Deadlift){
		
		List<String[]> schedule = new ArrayList<String[]>();
		String nameFile = fileName.getText().toString();

		//Calculation starts here
		List<String[]> mondaySquat = new ArrayList<String[]>();
		List<String[]> mondayBench = new ArrayList<String[]>();
		List<String[]> mondayRow = new ArrayList<String[]>();
		List<String[]> wedSquat = new ArrayList<String[]>();
        List<String[]> wedDeadlift = new ArrayList<String[]>();
        List<String[]> wedPress = new ArrayList<String[]>();
		List<String[]> friSquat = new ArrayList<String[]>();
		List<String[]> friBench = new ArrayList<String[]>();
		List<String[]> friRow = new ArrayList<String[]>();
        List<String[]> sched = new ArrayList<String[]>();

        Mondays monday = new Mondays();
        Fridays friday = new Fridays();
        Wednesdays wednesday = new Wednesdays();

        mondaySquat.add(monday.calcMon(Squat));
        mondayBench.add(monday.calcMon(Bench));
        mondayRow.add(monday.calcMon(Row));
        friBench.add(friday.calcBFriday(mondayBench));
        friSquat.add(friday.calcBFriday(mondaySquat));
        friRow.add(friday.calcBFriday(mondayRow));
        wedSquat.add(wednesday.wedBench(mondayBench));
        wedPress.add(wednesday.calcWed(oPress, 1.1));
        wedDeadlift.add(wednesday.calcWed(Deadlift, 1.080));

        sched.add(schedule(mondaySquat, mondayBench, mondayRow,
                            wedSquat, wedPress, wedDeadlift,
                            friSquat, friBench, friRow));

		createFile(nameFile, sched);
        //createFile("1", mondaySquat);
        //createFile("2", mondayBench);
        //createFile("3", mondayRow);
        //createFile("4", friBench);
		
	}



    private String[] schedule(List<String[]> mSquat, List<String[]> mBench, List<String[]> mRow,
    						List<String[]> wSquat, List<String[]> wPress, List<String[]> wDeadlift,
    						List<String[]> fSquat, List<String[]> fBench, List<String[]> fRow){

    	Integer iterMBench =0;
    	Integer iterMSquat=0;
    	Integer iterMRow=0;
    	Integer iterWSquat=0;
    	Integer iterWDeadlift=0;
    	Integer iterWPress=0;
    	Integer iterFSquat=0;
    	Integer iterFBench = 0;
    	Integer iterFRow=0;

        int countmb=1, countms=0, countmr=1,
        	countws=1, countwp=1, countwd=1,
        	countfs=1, countfb=1, countfr=1;

        String[] aMBench = mBench.get(0);
        String[] aMSquat = mSquat.get(0);
        String[] aMRow = mRow.get(0);
        String[] aWSquat = wSquat.get(0);
        String[] aWPress = wPress.get(0);
        String[] aWDeadlift = wDeadlift.get(0);
        String[] aFSquat = fSquat.get(0);
        String[] aFBench = fBench.get(0);
        String[] aFRow = fRow.get(0);

        String[] aSchedule = new String[575];
    	String state = "monday squat";


    	for(int i=0;i<aSchedule.length;i++){
    		switch(state){
    			case "monday squat":
    				while(iterMSquat < aMSquat.length){

    					if(countms != 0 && countms%5==0){
    						countms++;
                            aSchedule[i] = aMBench[iterMBench++];
    						state = "monday bench";
    						break;
    					}else {
                            countms++;
                            aSchedule[i] = aMSquat[iterMSquat++];
                            break;
                        }
					}
                    if(iterMSquat==aMSquat.length) {
                        state = "monday bench";
                       // aSchedule[i] = aBench[iterMBench];
                        aSchedule[i+1] = aMBench[iterMBench++];
                        i++;
                    }
    				break;
    			case "monday bench":
    				while(iterMBench<aMBench.length){

    					if(countmb!= 0 && countmb%5==0){
    						aSchedule[i] = aMRow[iterMRow++];
                            countmb++;
    						state = "monday row";
    						break;
    					}else {
                            countmb++;
                            aSchedule[i] = aMBench[iterMBench++];
                            break;
                        }
    				}
                    if(iterMBench==aMBench.length) {
                        aSchedule[i] = aMBench[iterMBench-1];
                        aSchedule[i+1] = aMRow[iterMRow++];
                        i++;
                        state = "monday row";
                    }
    				break;
    			case "monday row":
    				while(iterMRow<aMRow.length){
    				
    					if(countmr!=0 && countmr%5==0){
    						aSchedule[i] = "\n";
                            aSchedule[i+1] = aWSquat[iterWSquat++];
                            i++;
                            countmr++;
    						state = "wed squat";
    						break;
    					}else {
                            countmr++;
                            aSchedule[i] = aMRow[iterMRow++];
                            break;
                        }
    				}
    				if(iterMRow==aMRow.length) {
                        aSchedule[i] = aMRow[iterMRow-1];
                        aSchedule[i+1] = "\n";
                        aSchedule[i+2] = aWSquat[iterWSquat++];
                        i=i+2;
                        state = "wed squat";
                    }
                    break;
                case "wed squat":
	                while(iterWSquat < aWSquat.length){

	    					if(countws != 0 && countws%4==0){
	    						countws++;
	                            aSchedule[i] = aWPress[iterWPress++];
	    						state = "wed press";
	    						break;
	    					}else {
	                            countws++;
	                            aSchedule[i] = aWSquat[iterWSquat++];
	                            break;
	                        }
						}
	                    if(iterWSquat==aWSquat.length) {
	                        state = "wed press";
	                       // aSchedule[i] = aBench[iterMBench];
	                        aSchedule[i+1] = aWPress[iterWPress++];
	                        i++;
	                    }
	    				break;
	    			case "wed press":
		    			while(iterWPress<aWPress.length){

	    					if(countwp!= 0 && countwp%4==0){
	    						aSchedule[i] = aWDeadlift[iterWDeadlift++];
	                            countwp++;
	    						state = "wed deadlift";
	    						break;
	    					}else {
	                            countwp++;
	                            aSchedule[i] = aWPress[iterWPress++];
	                            break;
	                        }
	    				}
	                    if(iterWPress==aWPress.length) {
	                        aSchedule[i] = aWDeadlift[iterWDeadlift++];
	                        state = "wed deadlift";
	                    }
	    				break;
	    			case "wed deadlift":
		    			while(iterWDeadlift<aWDeadlift.length){
	    				
	    					if(countwd!=0 && countwd%4==0){
	    						aSchedule[i] = "\n";
	                            aSchedule[i+1] = aFSquat[iterFSquat++];
	                            i++;
	                            countwd++;
	    						state = "friday squat";
	    						break;
	    					}else {
	                            countwd++;
	                            aSchedule[i] = aWDeadlift[iterWDeadlift++];
	                            break;
	                        }
	    				}
	    				if(iterWDeadlift==aWDeadlift.length) {
	                        aSchedule[i] = aFSquat[iterFSquat++];
	                        state = "friday squat";
	                    }
	                    break;
	                case "friday squat":
		                while(iterFSquat < aFSquat.length){

	    					if(countfs != 0 && countfs%6==0){
	    						countfs++;
	                            aSchedule[i] = aFBench[iterFBench++];
	    						state = "friday bench";
	    						break;
	    					}else {
	                            countfs++;
	                            aSchedule[i] = aFSquat[iterFSquat++];
	                            break;
	                        }
						}
	                    if(iterFSquat==aFSquat.length) {
	                        state = "friday bench";
	                       // aSchedule[i] = aBench[iterMBench];
	                        aSchedule[i+1] = aFBench[iterFBench++];
	                        i++;
	                    }
	    				break;
	    			case "friday bench":
		    			while(iterFBench<aFBench.length){

	    					if(countfb!= 0 && countfb%6==0){
	    						aSchedule[i] = aFRow[iterFRow++];
	                            countfb++;
	    						state = "friday row";
	    						break;
	    					}else {
	                            countfb++;
	                            aSchedule[i] = aFBench[iterFBench++];
	                            break;
	                        }
	    				}
	                    if(iterFBench==aFBench.length) {
	                        aSchedule[i] = aFBench[iterFBench-1];
                            aSchedule[i+1] = aFRow[iterFRow++];
                            i++;
	                        state = "friday row";
	                    }
	    				break;
	    			case "friday row":
		    			while(iterFRow<aFRow.length){
	    				
	    					if(countfr!=0 && countfr%6==0){
	    						aSchedule[i] = "\n";
	                            aSchedule[i+1] = aMSquat[iterMSquat++];
	                            i++;
	                            countfr++;
	    						state = "monday squat";
	    						break;
	    					}else {
	                            countfr++;
	                            aSchedule[i] = aFRow[iterFRow++];
	                            break;
	                        }
	    				}

                        break;


    		}
    	}

       // aSchedule[182] = iterWed.toString();
       // aSchedule[183] = iterFBench.toString();
        //schedule.add(aSchedule);
        return aSchedule;
    }

	public void createFile(String fileName, List<String[]> content){
        fileName = fileName +".csv";
        //content = new ArrayList<String[]>();
        try
        {
            File root = new File(Environment.getExternalStorageDirectory(), "Lift to Lose");
            if (!root.exists()) {
                root.mkdirs();
            }
            File gpxfile = new File(root, fileName);
            FileWriter writer = new FileWriter(gpxfile);
            CSVWriter csvwrite = new CSVWriter(writer, CSVWriter.DEFAULT_SEPARATOR, ' ');
            //String[] data = content.toArray(new String[content.size()]);
            //data.add(content);
            csvwrite.writeAll(content);
            csvwrite.flush();
            csvwrite.close();
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }
}
