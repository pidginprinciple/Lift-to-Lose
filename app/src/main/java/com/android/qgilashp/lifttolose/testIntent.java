package com.android.qgilashp.lifttolose;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class testIntent extends Activity {
	TextView testintentText;
    TextView start;
    TextView end;
	@Override
    public void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);

    	this.setContentView(R.layout.testintview);
    	testintentText = (TextView) findViewById(R.id.filename);
        start = (TextView) findViewById(R.id.start);
        end = (TextView) findViewById(R.id.end);

    	Intent testInt = getIntent();
    	Bundle extras = testInt.getExtras();

    	if(extras!=null){
    		String showIt = (String) extras.get("filestart");
            int startIt = extras.getInt("start");
            int endIt = extras.getInt("end");
    		testintentText.setText(showIt);
            start.setText(String.valueOf(startIt));
            end.setText(String.valueOf(endIt));
    	}
    }
}
