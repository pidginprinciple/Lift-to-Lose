package com.android.qgilashp.lifttolose;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

/**
 * Created by Flor on 8/24/2014.
 */
public class mainAdapter extends BaseAdapter {

    ArrayList listofFiles;
    public final Activity context;
    private final String MY_PREFS_NAME = "files";
    private int currLine;
  //  String status;
   // private final Activity listactivity;

    public mainAdapter(Activity context, Map<String, String> list){
       // super(listactivity, R.layout.filerow, list);
        listofFiles = new ArrayList();
        listofFiles.addAll(list.entrySet());
        this.context = context;
    }

    @Override
    public long getItemId(int position){
        return 0;
    }

    @Override
    public Map.Entry<String, String> getItem(int position){
        return (Map.Entry) listofFiles.get(position);
    }

    @Override
    public int getCount(){
        return listofFiles.size();
    }

    static class ViewHolder {
    	protected TextView title;
    	protected TextView filedate;
    	protected Button start;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent){
        View tempview = null;
        final ViewHolder viewHolder = new ViewHolder();
        if(view == null){
            tempview = LayoutInflater.from(parent.getContext()).inflate(R.layout.filerow, parent, false);
            //view = inflate.inflate(R.layout.filerow, null);
            viewHolder.title = (TextView) tempview.findViewById(R.id.textView);
            viewHolder.filedate = (TextView) tempview.findViewById(R.id.textView2);
            viewHolder.start = (Button) tempview.findViewById(R.id.button);
            tempview.setTag(viewHolder);
        }else {
            tempview = view;
        }
        Map.Entry<String, String> item = getItem(position);
        ViewHolder holder = (ViewHolder) tempview.getTag();
        holder.title.setText(item.getKey());
        holder.filedate.setText(item.getValue());
        final String nameofFile = item.getKey();
        final String date = item.getValue();

        	SharedPreferences getDate = context.getSharedPreferences(MY_PREFS_NAME, 0);

            currLine = getDate.getInt(item.getKey()+"line", 0);
        	SimpleDateFormat format = new SimpleDateFormat("MM-dd-yyyy");
        	final Date currDate = new Date();
            Date dateSaved = new Date();
            final String setcurrDate = format.format(new Date());
        if(item.getValue() != "Did not start yet." || item.getValue()!= "Finished") {
            try {
                String dateParse = getDate.getString(item.getKey()+"nextdate", "01-01-1111");
                dateSaved = format.parse(dateParse);
                dateSaved = addDays(dateSaved, 1);
            } catch (ParseException ex) {
                ex.printStackTrace();
            }
        }
        //((TextView) tempview.findViewById(R.id.textView)).setText(item.getKey());
        //((TextView) tempview.findViewById(R.id.textView2)).setText(item.getValue());
       //(Button) view.findViewById(R.id.button);
        if(item.getValue() == "Did not start yet." || currDate.compareTo(dateSaved) == 0
        	|| currDate.compareTo(dateSaved) > 0){
        	// disable the button 
        	holder.start.setEnabled(true);
        }else if(currLine==36){         // if workout is finished mark it and disable the button forever
        	holder.start.setEnabled(false);
            getDate.edit().putString(item.getKey()+"nextdate", "Finished");
        }else{
            holder.start.setEnabled(false);
        }

        holder.start.setOnClickListener(new View.OnClickListener() {

        	@Override
        	public void onClick (View v) {
        		if(date == "Did not start yet."){
        			setStartDate(nameofFile);
        			Intent intent = new Intent(context, liftHub.class);
        			intent.putExtra("file", nameofFile); 			
        			context.startActivity(intent);
        		}else{
        			Intent intent = new Intent(context, liftHub.class);
        			intent.putExtra("file", nameofFile);
        			clearButtons(nameofFile);
        			context.startActivity(intent);
        		}
        	}
        });

        return tempview;
    }

    public void setStartDate(String filesave){
    	String date = new SimpleDateFormat("MM-dd-yyyy").format(new Date());
    	SharedPreferences.Editor dateStarted = context.getSharedPreferences(MY_PREFS_NAME, 0).edit();
    	dateStarted.putString(filesave, date);
    	dateStarted.apply();
    }

    public static Date addDays(Date date, int days){
    	Calendar cal = Calendar.getInstance();
    	cal.setTime(date);
    	cal.add(Calendar.DATE, days);
    	return cal.getTime();
    }

    private void clearButtons(String file){
    	SharedPreferences.Editor buttons = context.getSharedPreferences(MY_PREFS_NAME, 0).edit();
        buttons.putBoolean(file+"b1", false);
        buttons.putBoolean(file+"b2", false);
        buttons.putBoolean(file+"b3", false);
        buttons.apply();
    }

}
