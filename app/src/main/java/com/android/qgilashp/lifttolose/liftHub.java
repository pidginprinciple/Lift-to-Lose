package com.android.qgilashp.lifttolose;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.qgilashp.lifttolose.Webviews.mainInstruction;
import com.android.qgilashp.lifttolose.Webviews.mainWebview;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class liftHub extends Activity {

    private final String MY_PREFS_NAME = "files";
    public Button first;
    public Button second;
    public Button third;
    public Button delete;
    public Button helpButton;
    private ImageButton aboutButton;
    private TextView ftext;
    private TextView stext;
    private TextView ttext;
    private TextView wtext;
    private ProgressBar progress;
    public Button finish;
    private String showIt; //filename
    private int currLine;
    private int week;
    private int setNum = 5; // number of sets in current workout
    private Boolean checkedOne = false;
    private Boolean checkedTwo = false;
    private Boolean checkedThree = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.setContentView(R.layout.hub);
        first = (Button) findViewById(R.id.first);
        second = (Button) findViewById(R.id.second);
        third = (Button) findViewById(R.id.third);
        delete = (Button) findViewById(R.id.deletebutton);
        helpButton = (Button) findViewById(R.id.helpbutton);
        aboutButton = (ImageButton) findViewById(R.id.imageView);
        ftext = (TextView) findViewById(R.id.fbuttext);
        stext = (TextView) findViewById(R.id.sbuttext);
        ttext = (TextView) findViewById(R.id.tbuttext);
        wtext = (TextView) findViewById(R.id.week);
        progress = (ProgressBar) findViewById(R.id.progressBar);
        finish = (Button) findViewById(R.id.finish);
        finish.setVisibility(View.GONE);

        getFileData();
        if (savedInstanceState != null) {
            showIt = savedInstanceState.getString("param");
            checkedOne = savedInstanceState.getBoolean("firstb");
            checkedTwo = savedInstanceState.getBoolean("secb");
            checkedThree = savedInstanceState.getBoolean("thirdb");
        }

        setLiftSets();
        getSetNum();
        saveinfo();
        checkButtons();
        clickButtons();

        week += 1;
        wtext.setText("Week " + week);
        progress.setMax(36);
        progress.setProgress(currLine);
    }

    @Override
    public void onPause() {
        super.onPause();
        getFileData();
        saveinfo();

    }

    @Override
    public void onResume() {
        super.onResume();

        setLiftSets();
        getFileData();
        getExtrasButtons();
        checkButtons();
        clickButtons();
    }

    @Override
    protected void onSaveInstanceState(Bundle icicle) {
        super.onSaveInstanceState(icicle);
        icicle.putString("param", showIt);
        icicle.putBoolean("firstb", checkedOne);
        icicle.putBoolean("secb", checkedTwo);
        icicle.putBoolean("thirdb", checkedThree);
    }

    private void saveinfo() {
        SharedPreferences.Editor buttons = getSharedPreferences(MY_PREFS_NAME, 0).edit();
        if (checkedOne)
            buttons.putBoolean(showIt + "b1", checkedOne);
        if (checkedTwo)
            buttons.putBoolean(showIt + "b2", checkedTwo);
        if (checkedThree)
            buttons.putBoolean(showIt + "b3", checkedThree);
        if (currLine % 3 == 0) {
            int storeweek = Math.round((currLine / 3));
            buttons.putInt(showIt + "week", storeweek);
        }
        buttons.apply();
    }

    private void sendNextLift() { // set the next date for workout
        SharedPreferences.Editor line = getSharedPreferences(MY_PREFS_NAME, 0).edit();
        line.putInt(showIt + "line", currLine + 1);
        line.apply();  // add 1 for the next lift session

        String date = new SimpleDateFormat("MM-dd-yyyy").format(new Date());
        SharedPreferences.Editor nextDate = getSharedPreferences(MY_PREFS_NAME, 0).edit();
        nextDate.putString(showIt + "nextdate", date);
        nextDate.apply(); // set current date and save it for next session

        setSetNum(); // set the number of sets for the next session

        Intent intent = new Intent(this, bricsMain.class);  // go back to the menu
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        this.startActivity(intent);
        this.finish();
    }

    private void getFileData() {
        Intent testInt = getIntent();
        Bundle extras = testInt.getExtras();

        if (extras != null)
            showIt = (String) extras.get("file");

        SharedPreferences.Editor setIt = getSharedPreferences(MY_PREFS_NAME, 0).edit();
        setIt.putString("currfile", showIt);
        setIt.apply();

        SharedPreferences getLine = getSharedPreferences(MY_PREFS_NAME, 0);
        currLine = getLine.getInt(showIt + "line", 0);    // get the filename's lin
        week = getLine.getInt(showIt + "week", 0);

    }

    private void setLiftSets() {

        if (currLine == 1 || currLine == 4 || currLine == 7 || currLine == 10 || currLine == 13 ||
                currLine == 16 || currLine == 19 || currLine == 22 || currLine == 25 || currLine == 28
                || currLine == 31 || currLine == 34) {

            ftext.setText("Start Squat");
            stext.setText("Start Press");
            ttext.setText("Start Deadlift");
        } else {
            ftext.setText("Start Squat");
            stext.setText("Start Bench");
            ttext.setText("Start Row");
        }
    }

    public void clickButtons() {
        // first button
        first.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (setNum) {
                    case 4:
                        firstButton(0, 4); // commit start and end range
                        break;
                    case 3:
                        firstButton(0, 3);
                        break;
                    case 7:
                        firstButton(0, 5);
                        break;
                }
            }
        });

        second.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (setNum) {
                    case 4:
                        secondButton(5, 9); // commit start and end range
                        break;
                    case 3:
                        secondButton(4, 7);
                        break;
                    case 7:
                        secondButton(6, 11);
                        break;
                }
            }
        });

        third.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (setNum) {
                    case 4:
                        thirdButton(10, 14); // commit start and end range
                        break;
                    case 3:
                        thirdButton(8, 11);
                        break;
                    case 7:
                        thirdButton(12, 17);
                        break;
                }
            }
        });

        aboutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                Intent intent = new Intent(getBaseContext(), mainWebview.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getBaseContext().startActivity(intent);
            }
        });

        helpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), mainInstruction.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("page", 2);
                getBaseContext().startActivity(intent);
            }
        });

        /**
         *  finish the workout session, add 1 to line for the next day
         *  and update date for next session
         **/

        finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendNextLift();
            }
        });

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder alert = new AlertDialog.Builder(liftHub.this);
                alert.setTitle("WARNING:FILE DELETION");
                alert.setMessage("This will delete this file and all progress with it.");
                alert.setCancelable(true);
                alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                //System.exit(0);
                                dialog.cancel();
                            }
                        });
                        alert.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                File root = new File(Environment.getExternalStorageDirectory(), "Lift to Lose");

                                if (!root.exists()) {
                                    Toast.makeText(liftHub.this, "Folder does not Exist", Toast.LENGTH_SHORT).show();
                                }
                                File filedelete = new File(root, showIt);
                                try {
                                    delete(filedelete);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }


                                SharedPreferences.Editor setIt = getSharedPreferences(MY_PREFS_NAME, 0).edit();
                                setIt.remove(showIt + "line");
                                setIt.remove(showIt + "week");
                                setIt.remove(showIt + "nextdate");
                                setIt.remove(showIt + "b1");
                                setIt.remove(showIt + "b2");
                                setIt.remove(showIt + "b3");
                                setIt.apply();

                                goBack();

                            }
                        });

                AlertDialog alert1 = alert.create();
                alert1.show();

            }
        });

          /*  finish.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                            if(isChecked){sendNextLift();}
                        }
                    });*/

    }

    public void goBack(){
        Intent second = new Intent(this, bricsMain.class);
        this.startActivity(second);
    }
    public void setSetNum() {
        SharedPreferences.Editor setIt = getSharedPreferences(MY_PREFS_NAME, 0).edit();

        switch (setNum) {
            case 4:
                setIt.putInt(showIt + "setNum", 3);
                setIt.apply();
                break;
            case 3:
                setIt.putInt(showIt + "setNum", 7);
                setIt.apply();
                break;
            case 7:
                setIt.putInt(showIt + "setNum", 4);
                setIt.apply();
                break;
        }

    }

    public void getSetNum() {

        SharedPreferences getLine = getSharedPreferences(MY_PREFS_NAME, 0);
        setNum = getLine.getInt(showIt + "setNum", 4);
    }

    public void getExtrasButtons() {
        Intent check = getIntent();
        Bundle buttons = check.getExtras();
        SharedPreferences getLine = getSharedPreferences(MY_PREFS_NAME, 0);
        if (buttons != null) {

            checkedOne = getLine.getBoolean(showIt + "b1", false);
            if (!checkedOne) {
                checkedOne = buttons.getBoolean("firstbutton");
            }
            checkedTwo = getLine.getBoolean(showIt + "b2", false);
            if (!checkedTwo) {
                checkedTwo = buttons.getBoolean("secondbutton");
            }
            checkedThree = getLine.getBoolean(showIt + "b3", false);
            if (!checkedThree) {
                checkedThree = buttons.getBoolean("thirdbutton");
            }
        }
    }

    public void checkButtons() {

        if (checkedOne) {
            first.setEnabled(false);
            ftext.setText("");
        }
        if (checkedTwo) {
            second.setEnabled(false);
            stext.setText("");
        }
        if (checkedThree) {
            third.setEnabled(false);
            ttext.setText("");
        }
        if (checkedOne && checkedTwo && checkedThree)
            finish.setVisibility(View.VISIBLE);
    }

    public void firstButton(int start, int end) {
        Intent first = new Intent(this, lifttoloser.class);
        first.putExtra("filestart", showIt);
        first.putExtra("start", start);
        first.putExtra("end", end);
        first.putExtra("Set", "firstbutton");
        this.startActivity(first);
    }

    public void secondButton(int start, int end) {
        Intent second = new Intent(this, lifttoloser.class);
        second.putExtra("filestart", showIt);
        second.putExtra("start", start);
        second.putExtra("end", end);
        second.putExtra("Set", "secondbutton");
        this.startActivity(second);
    }

    public void thirdButton(int start, int end) {
        Intent third = new Intent(this, lifttoloser.class);
        third.putExtra("filestart", showIt);
        third.putExtra("start", start);
        third.putExtra("end", end);
        third.putExtra("Set", "thirdbutton");
        this.startActivity(third);
    }

    void delete(File f) throws IOException {
        if (f.isDirectory()) {
            for (File c : f.listFiles()) {
                delete(c);
            }
        } else if (f.getAbsolutePath().endsWith("csv")) {
            if (!f.delete()) {
                new FileNotFoundException("Failed to delete file: " + f);
            }
        }
    }
}
