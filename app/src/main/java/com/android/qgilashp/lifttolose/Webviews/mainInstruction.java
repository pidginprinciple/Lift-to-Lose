package com.android.qgilashp.lifttolose.Webviews;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;

import com.android.qgilashp.lifttolose.R;
import com.android.qgilashp.lifttolose.Util.ViewPagerAdapter;

/**
 * Created by Flor on 3/2/2015.
 */
public class mainInstruction extends Activity{

    private ViewPagerAdapter ViewPageAdapter;
    private ViewPager ViewPager;
    private int pagenum;
    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.instructionpager);
        ViewPageAdapter = new ViewPagerAdapter(this);

        Intent page = getIntent();
        Bundle pager = page.getExtras();

        if(pager == null){
            pagenum = 0;
        }else {
            pagenum = pager.getInt("page");
        }
        ViewPager = (ViewPager) findViewById(R.id.pager);
        ViewPager.setAdapter(ViewPageAdapter);
        ViewPager.setCurrentItem(pagenum);
    }
}
