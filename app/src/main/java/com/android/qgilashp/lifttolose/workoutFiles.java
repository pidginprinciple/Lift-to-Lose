package com.android.qgilashp.lifttolose;



/**
 * Created by Flor on 8/25/2014.
 */
public class workoutFiles {
    String fileName;
    String status = "Not Completed";
    Boolean completed = false;

    public workoutFiles(){

    }

    // Constructor

    public workoutFiles(String fileName, String status, Boolean completed){
        this.fileName = fileName;
        this.completed = completed;
        this.status = status;
    }
    // File names
    public String getFileName(){
        return fileName;
    }

    public void setFileName(String setFile){
        this.fileName = setFile;
    }

    // status

    String getStatus(){
        return status;
    }

    public void setStatus(String setStat){
        this.status = setStat;
    }

    //is it completed?

    public void setComplete(Boolean check){
        this.completed = check;
    }
}
